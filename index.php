<?php $url= "guardar.php"; ?>
<!DOCTYPE html>
<html>
<head> 
    <meta charset="UTF-8">
	<meta name="viewport" content="initial-scale=1.0">
    <title>Maps JavaScript API</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
	<style> 
  	  #map {
        height: 80%;
      }
     
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
	</style> 
</head>  
	<body>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <div id ="map"> </div> 
        <input type="text" value="" id="cordenadas" name="cordenadas">
        
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAvVm3RVDSTNaqUvatku80k3227WmaEgJQ&signed_in=true&callback=initMap" async defer></script>
	<script>
	      var map;
  	 function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
		  center: {lat:-17.3938831, lng: -66.2339164},
          zoom: 10,
        });
       
        google.maps.event.addListener(map, 'click', function(event) {
          
  placeMarker(map, event.latLng);
  });

function placeMarker(map, location) {
  var marker = new google.maps.Marker({
    position: location,
    map: map
  });
  var infowindow = new google.maps.InfoWindow({
    content: 'Latitude: ' + location.lat() +
    '<br>Longitude: ' + location.lng()
  });
  
  infowindow.open(map,marker);

  document.getElementById("cordenadas").value=location;
  var cordenada_marcada = document.getElementById("cordenadas").value;
  $.ajax({
    url: "<?=$url?>",
    data:{
      punto:cordenada_marcada,
    },
    type: "GET",
  })
  .done(function(data){
    $("#tabla").html(data);
  })
}
      }
	</script>
  <div class="container">
  <table class="table table-bordered" id="tabla">
  <thead>
     <tr>
        <th>n°</th>
        <th scope="col">Punto Cordenada</th>
        <th scope="col">Fecha</th>
        <th scope="col">Hora</th>
     </tr>
    </thead>
  <?php
  require("coneccion.php");
  $consulta="select * from coordenada";
  $query=mysqli_query($connec,$consulta);
   $c=1;
  while ($row=$query->fetch_array()) {
   
  ?>
  <tr>
  <td><?php echo $c++;?></td>
  <td><?php echo $row['punto_coordenada'];?></td>
  <td><?php echo $row['fecha'];?></td>
  <td><?php echo $row['hora'];?></td>
  </tr>

  <?php

  }
  ?>
  </table>
  </div>
	</body> 
</html>